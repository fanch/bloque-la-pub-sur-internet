var header 	= document.getElementById("header");
var title 	= document.getElementById("title");
var content	= document.getElementById("container");
var browser = navigator.userAgent.toLowerCase();
var minPad = 20;

if (browser.indexOf('firefox') > -1) {
header.style.position = "fixed";
}

function resize()
{
	pad = window.innerHeight*.28 - title.clientHeight/2;

	content.style.marginTop 	= window.innerHeight*2/3 + "px";
	content.style.paddingTop 	= window.innerHeight*2/5 + "px";

	scroll();
}

function scroll()
{
	var scrolled = document.documentElement.scrollTop;
	var top =	 pad 	- scrolled;
	var bottom = pad*2 	- scrolled;

	header.style.paddingTop = ( top < minPad ? minPad : top ) + "px";
	header.style.paddingBottom = ( bottom < minPad ? minPad : ( pad < bottom ? pad : bottom ) ) + "px";
}

if (browser.indexOf('firefox' || 'chrome') > -1) {
    onresize = resize;
	onscroll = scroll;
	resize();
}

/* if (browser.indexOf('firefox') > -1) */